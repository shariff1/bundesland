// Observable Version
import {Component, OnInit} from '@angular/core';
import {Hero} from './capital';
import {HeroService} from '../service/bundesland.service';

@Component({
    selector: 'hero-list',
    templateUrl: '/app/src/html/bundesland.component.html',
    providers: [HeroService],
    styles: ['.error {color:red;}'],
})

export class BundesLandListComponent implements OnInit {
    errorMessage: string;
    heroes: Hero[];
    mode = 'Observable';

    constructor(private heroService: HeroService) {
    }

    ngOnInit() {
        this.getCapitals();
    }

    getCapitals() {
        this.heroService.getBundesland()
            .subscribe(
                heroes => this.heroes = heroes,
                error => this.errorMessage = <any>error);
    }

    addHero(name: string) {
        if (!name) {
            return;
        }
        this.heroService.create(name)
            .subscribe(
                hero => this.heroes.push(hero),
                error => this.errorMessage = <any>error);
    }


}


